using MPI, VMEC, StellaratorOptimizationMetrics, Printf

MPI.Init()

# The value of this input file should be changed with each iteration
nml = VMEC.read_vmec_namelist("input.aten_b94")

vmec = VMEC.run_vmec(MPI.COMM_WORLD, nml)

surface_labels = 0.1:0.1:0.9

quasisymmetry_values = Vector{Float64}(undef, length(surface_labels))
growth_rate_values = similar(quasisymmetry_values)
magnetic_well_values = similar(quasisymmetry_values)

target_qs_m_mode, target_qs_n_mode = (1, 4)
qs_eval_grid_size = (16, 16)
n_wells = 15
n_points = 1536

for (i, s) in enumerate(surface_labels)
    vmec_surface = VmecSurface(s, vmec)
    quasisymmetry_values[i] = quasisymmetry_deviation(target_qs_m_mode, target_qs_n_mode,
                                                      qs_eval_grid_size[1], qs_eval_grid_size[2],
                                                      vmec_surface)
    magnetic_well_values[i] = magnetic_well(vmec_surface)
    growth_rate_values[i] = StellOptMetrics.cobra_opt(vmec_surface, n_wells; np = n_points)[1]
end

fio = open("iteration_out.txt", "w")
for i in eachindex(surface_labels)
    write_line = sprint(print, "s = ", surface_labels[i], "Quasisymmetry Deviation: ", quasisymmetry_values[i],
                               "Magnetic well: ", magnetic_well_values[i], 
                               "Ballooning growth rate: ", growth_rate_values[i], '\n';
                               context=:compact => true)
    write(fio, write_line)
end

close(fio)

